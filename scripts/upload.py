from sqlalchemy import create_engine
import pandas as pd 
import logging


def create_engine_(user:str = "postgres", 
                    password:str = "postgres", 
                    host:str = "localhost", 
                    database:str = "postgres"): 
    engine = create_engine(f"postgresql://{user}:{password}@{host}/{database}")
    return engine

def save_to_bucket(dataframe:"pd.DataFrame" = None, save_path:str = None): 
    """La logica implicaría que los datos que se enviían a una base en postgres tambien se envien a un bucket 
    para tener seguridad  y consistencia en los datos"""
    try: 
        dataframe.to_csv(save_path, index = False)
    except Exception as e: 
        logging.exception("Can not save data due to {}")



def save_to_sql(dataframe:"pd.DataFrame" = None, table_name:str = None, columns:list = None, if_exists:str = "replace"): 
    try: 
        if not columns: 
            dataframe.to_sql(table_name, con = create_engine_(), if_exists= if_exists)
        else: 
            dataframe[columns].to_sql(table_name, con = create_engine_(), if_exists= if_exists)
        return True
    except Exception as e: 
        logging.exception(f"Can not  save data to Postgresql due to {e}")
        return False