from concurrent.futures import process
from extract import extract_data, save_data
from upload import save_to_sql, save_to_bucket, create_engine_
from preprocess import preprocess, agregate_data
from datetime import datetime
import sys 
import logging
import os 




CRYPTO = [
    "bitcoin", 
    "cardano", 
    "ethereum"
]

date = str(datetime.today())[:10]

args = sys.argv


if args[1] == "complete": 
    params = {
        "initial_date" : "2022-01-01", 
        "final_date" : date
    }
if args[1] == "daily": 
    params = {
        "date": date, 
        "days": args[3]
    }

data_type = args[2]

for coin in CRYPTO: 

    # Extract Data 
    try: 
        logging.info("Saving Data to postgresql")

        data = extract_data(info_type = data_type, 
                            coin =coin, 
                            **params)
        complete_save_path = save_data(data[0], date= date, 
                                        coin = coin, 
                                        data_type = data_type)
        if complete_save_path: 
            logging.info("Data store in local")
    except Exception as e: 
        logging.exception(f"Can not save data into postgresql due to: {e}")

    # Preprocess Data only for price Option. To store data into postgresql
    try: 
        logging.info("Preprocess Data")
        data = preprocess(saved_file_path=complete_save_path, coin = coin)
    except Exception as e: 
        logging.exception(f"Can not save data into postgresql due to: {e}")

    # Save Data historic
    try: 
        logging.info("Saving Data to postgresql")
        base_path = "/Users/viktorpimentel/Documents/mutt_data/exam-victor-pimentel/data/process_data"
        save_path = "{coin}_{date}.csv".format(coin = coin, date = date.replace("-","_"))
        preprocess_data_path = os.path.join(base_path, save_path)

        saved_bucket = save_to_bucket(dataframe= data, save_path = preprocess_data_path)
        saved_sql = save_to_sql(dataframe=data,table_name = "history_data", columns = None)
        if saved_sql: print(f"Data raw saved for {coin}")

    except Exception as e: 
        logging.exception(f"Can not save data into postgresql due to: {e}")

    # Save Aggregated Data: 
    try: 
        logging.info("Creating Aggregating Data")
        data = agregate_data(query = "SELECT * FROM public.history_data", connection =create_engine_())
        data["upload_date"] = str(datetime.today)[:10]
        saved_sql = save_to_sql(dataframe=data,table_name = "aggregated_data", columns = [
            "price", 
            "coin_id", 
            "month", 
            "year", 
            "monthly_max", 
            "monthly_min", 
            "fecha_iso", 
            "fecha_unix"
        ], if_exists= "append")
        if saved_sql: print(f"Data Aggregated saved for {coin}")
    except Exception as e: 
        logging.exception(f"Can not save aggregated data into postgresql database")


    

    



