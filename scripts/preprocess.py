from datetime import datetime
from numpy import save
import pandas as pd 
import json
import logging
from sqlalchemy import text

def read_file(saved_file_path): 
    with open (saved_file_path, "r") as json_file: 
        data = json.load(json_file)
    return data

def convert_unix_to_date(dataframe:"pd.DataFrame" = None, column:str = None): 
    dataframe["fecha_iso"] = dataframe[column].apply(lambda x: str(datetime.fromtimestamp(int(str(x)[:10])))[:10])
    return dataframe


def get_historic_prices(saved_file_path, coin:str = None): 
    prices = pd.DataFrame()
    try: 
        data = read_file(saved_file_path)
        prices = pd.DataFrame(data["prices"], columns = ["fecha_unix", "price"])
        prices["raw_data"] = str(data)
        prices["coin_id"] = coin
        prices = convert_unix_to_date(prices, "fecha_unix")
    except Exception as e:
        logging.exception(f"Can not load data into DataFrame due to {e}")
    return prices
    

def preprocess(saved_file_path, coin:str = None):
    data = pd.DataFrame() 
    try: 
        data = get_historic_prices(saved_file_path, coin = coin)
    except Exception as e: 
        logging.exception(f"No valid extraccion for preprocessing due to {e}")
    return data


def agregate_data(query:str = None, connection = None): 
    assert connection is not None, "Please provide a valid Conenction engine"
    
    if not query: 
        query = text("""
            SELECT * FROM postgres.history_data
        """)
    data_aggregated = pd.read_sql(query, con = connection)
    data_aggregated["month"] = pd.to_datetime(data_aggregated["fecha_iso"], format = "%Y-%m-%d").apply(lambda x: x.month)
    data_aggregated["year"] = pd.to_datetime(data_aggregated["fecha_iso"], format = "%Y-%m-%d").apply(lambda x: x.year)

    maximum = data_aggregated.merge(data_aggregated.groupby(["coin_id", "month"]).max().reset_index()[["month", "price"]], how = "left", on = "month").rename(columns = {
        "price_y": "monthly_max", 
        "price_x": "price"
    })

        
    minimum = maximum.merge(data_aggregated.groupby(["coin_id", "month"]).min().reset_index()[["month", "price"]], how = "left", on = "month").rename(columns = {
        "price_y": "monthly_min", 
        "price_x": "price"
    })
    return  minimum