
from sqlalchemy import text
import pandas as pd 
from scripts import create_engine
import logging

def create_query(query): 
    return text(query)

def extract_data(query, engine): 
    try: 
        return pd.read_sql(query, engine)
    except Exception as e: 
        logging.exception(f"Can not  get data from Database due to {e}")
        return pd.DataFrame()

if __name__ == "__main__": 
    engine = create_engine(
        user = "postgres",
        password = "postgres", 
        host = "localhost", 
        database= "postgres"
    )

    query = create_query("""
    SELECT 
        AVG(price), 
        coin_id
    FROM
        public.aggregated_data
    GROUP BY
        coin_id
    """)

    data = extract_data(query = query, con = engine)
    print(len(data))

