import requests
import logging
import datetime
from datetime import timedelta



class GeckoData(object): 
    def __init__(self, version:str = None): 
        """
        Isntanciate Gecko Class for extract data from API


        La clase permite  extraer la información actual y el historico. Esto se lofra a través de la selección del endpoint en la propiedad self.endpoints

        La primera opcion sobre el simple/price se desarrollo como testing de la funcionalidad de la aplicacion. 
        """
        if version is None: 
            version = "v3"
        self.basic_endpoint = "https://api.coingecko.com/api/{version}".format(version = version)
        self.session = requests.session()
        self.endpoints = {
            "actual": "simple/price", 
            "history": "coins/{coin}/history"
        }
        self.headers = {
            'accept': 'application/json',
        }

    def get_info(self, info_type:str = None, **kargs): 
        """
        Funcion que permite extraer información desde la API de Gecko recibiendo cualquier tipo de parámetros para la petición requests. Sin embargo hace obligatorio
        especificar que tipo de extracción se requiere. Si es una extracción actual o periodica. 

        Parametros: 
        -----------

        info_type: str
            Recibe dos posibles opciones: 
                - history
                - actual

        En el caso de utilizar history los parametros adicionales son: 
            coin:str 
            date: str OR list

        Si se utiliza el parametro actual los parametros obligatorios son: 
            - coin:str Nombre de la crypto que se espera extraer
            - vs_currencies: Moneda en la cual se espera obtener su valor. 
        
        Returns: 
        -----------
        Lista con la salida de gecko para el tipo de información
        """
        logging.info(f"Get Information from Gekko")
        assert info_type is not None, "Please provide a valid info_type"
        params = tuple((i, kargs[i]) for i in kargs)
        url = self.basic_endpoint +  "/" + self.endpoints[str(info_type)]
        try: 
            if info_type == "history": 
                url = url.format(coin =kargs["id"])
            api_response = self.session.get(url = url, 
                                            params = params, 
                                            headers = self.headers)
            self.response = api_response
            self.response_json = api_response.json()
        except Exception as e: 
            logging.exception(f"Can not get prices information due to {e}")
        return self

def convert_str_to_date(str_date:str = None): 
    basic_date = ""
    try: 
        _ = datetime.datetime.strptime(str_date, "%Y-%m-%d")
        basic_date = "{day}-{month}-{year}".format(
            year = _.year, 
            month = _.month, 
            day = _.day
        )
    except Exception as e:
        logging.info("Can not convert date due to {e}")
    return basic_date


def create_list_of_dates(initial_date:str = None, final_date:str = None): 
    base = datetime.datetime.strptime(initial_date, "%Y-%m-%d")
    final = datetime.datetime.strptime(final_date, "%Y-%m-%d")
    num_days = (final - base).days
    date_list = [str(final - datetime.timedelta(days=x))[:10] for x in range(num_days)]
    return date_list

def save_data(json_dict:dict = None, date:str = None): 
    try: 
        with open("data_{date}.json".format(date = date.replace("-","_")), "w") as file: 
            file.write(str(json_dict))
        return True
    except Exception as e: 
        logging.exception(f"Can not save data due to {e}")
        return False


def extract_data(**kwargs,): 
    assert "coin" in kwargs.keys(), "Pass a valid Coin id"
    coins = {
        "bitcoin": "btc", 
        "ether": "eth", 
        "cardano":"ada"
    } 
    output_data = list()
    client = GeckoData()

    if kwargs["info_type"] == "history": 
        if "initial_date" in kwargs.keys() and "final_data" in kwargs.keys(): 
            try: 
                date_list = create_list_of_dates(initial_date = kwargs.get("initial_date"), 
                                                 final_date = kwargs.get("final_date"))
                logging.info(f"Número de días: {len(date_list)}")
            except Exception as e: 
                logging.exception("Can not create list of dates due to {e}")
                return output_data
        else: 
            if "date" in kwargs.keys():
                try: 
                    date_list = kwargs.get("date").split("")
                except Exception as e:
                    logging.exception("Can not create list of date due to {e}")
                    return output_data
        for date in date_list: 
            date = convert_str_to_date(str(date))
            client.get_info(info_type = kwargs["info_type"], id = kwargs["coin"],  date = date)
            internal_response = client.response_json
            output_data.append(internal_response)
    if kwargs["info_type"] == "actual": 
        client.get_info(info_type= kwargs["info_type"], ids = kwargs["coin"], vs_currencies =kwargs["vrs_currencies"] )
        output_data = client.response_json
    return output_data


if __name__ == "__main__": 
    import sys
    import os
    import json
    """
    La función Extract_data permite definir los parametros como segun sea necesaarios dentro de las diferentes opciones de extraccion. 
    De esta forma es posible no tener que crear una nueva función para el bulck process
    """
    args = sys.argv
    coin = args[1]
    date = args[2]
    initial_date = date.split(" ")[0]
    final_date = date.split(" ")[1]
    data = extract_data(info_type = "history", coin = coin, 
                        initial_date = initial_date, 
                        final_date = final_date)
    save_data(data, date= initial_date)
    
    

        
        

    