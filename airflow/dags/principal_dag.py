from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from datetime import datetime
from airflow.scripts import extract
from scripts import extract_data, preprocess



with DAG("exercise_dag", start_date = datetime(2022, 1, 1), # comenzara desde el primero de Enero de 2022
        schedule_interval= "0 3 * * *", catchup = True) as dag:
        
        # Extract Data for bitcoin
        extract_data_bitcoin = PythonOperator(
            task_id = "extract_data",
            provide_context = True,
            python_callable = extract_data, 
            op_kwargs = {"coin": "bitcoin", "info_type": "history"}
        ) 

        # Extract Data for Ada
        extract_data_bitcoin = PythonOperator(
            task_id = "extract_data",
            provide_context = True,
            python_callable = extract_data, 
            op_kwargs = {"coin": "ada", "info_type": "history"}
        ) 

        # Extract Data for Ethereum
        extract_data_bitcoin = PythonOperator(
            task_id = "extract_data",
            provide_context = True,
            python_callable = extract_data, 
            op_kwargs = {"coin": "eth", "info_type": "history"}
        ) 

    




    



