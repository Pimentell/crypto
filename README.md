


To add Postgresql to Docker 


- docker run --name main_table -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres 

Esto comienza una base de datos de postgres con los puertos abiertos para conectarse. Es una solución mucho mas sencilla visualmente utilizar Docker Client


![image](images/docker.jpg)



Una vez se tiene montada la base de datos en el docker y accesible. Es posible comenzar con los cronjobs para la extracción, transformación y carga de los datos en ella. 

El primer paso que se llevó a cabo fue la creación de una clase GeckoData para manera la interacción con el API desde donde se estará extrayendo la información. Adicionalmente, un par de funciones dentro del mismo documento que permiten la selección y la definición de los parámetros. De esta forma, a través de la función extract_data es posible hacer solicitudes a tres endpoints diferentes: 

1. History
2. Simple Price 
3. Market Chart

Para el presente ejercicio solo se utilizará la opción de extracciónd e market Chart. Que ofrece la posibilidad de extraer un historico de hasta 30 días de datos para cualquiera de las monedas que se encuenten en su plataforma. Utilizaremos tres: 

- Bitcoin
- Cardano
- Ethereum 

La función recibe como parametros de entrada: 

```python
    params = {
        "date": date, 
        "days": args[3]
    }

    data = extract_data(info_type = data_type, 
                        coin =coin, 
                        **params)
```

Los argumentos adicionales se pasan a través del cronjob y son tres: 

1. Tipo de extracción: Historica o diaría
2. Tipo información: Precio o historia
3. Número de días



Lo anterior se ejecuta a través de un archivo adicional. Inicialmente simulando la creación de un flujo de ETL (Airflow kind) que tiene la siguiente forma: 

```python
from concurrent.futures import process
from extract import extract_data, save_data
from upload import save_to_sql, save_to_bucket, create_engine_
from preprocess import preprocess, agregate_data
from datetime import datetime
import sys 
import logging
import os 

CRYPTO = [
    "bitcoin", 
    "cardano", 
    "ethereum"
]

date = str(datetime.today())[:10]

args = sys.argv


if args[1] == "complete": 
    params = {
        "initial_date" : "2022-01-01", 
        "final_date" : date
    }
if args[1] == "daily": 
    params = {
        "date": date, 
        "days": args[3]
    }

data_type = args[2]

for coin in CRYPTO: 

    # Extract Data 
    try: 
        logging.info("Saving Data to postgresql")

        data = extract_data(info_type = data_type, 
                            coin =coin, 
                            **params)
        complete_save_path = save_data(data[0], date= date, 
                                        coin = coin, 
                                        data_type = data_type)
        if complete_save_path: 
            logging.info("Data store in local")
    except Exception as e: 
        logging.exception(f"Can not save data into postgresql due to: {e}")

    # Preprocess Data only for price Option. To store data into postgresql
    try: 
        logging.info("Preprocess Data")
        data = preprocess(saved_file_path=complete_save_path, coin = coin)
    except Exception as e: 
        logging.exception(f"Can not save data into postgresql due to: {e}")

    # Save Data historic
    try: 
        logging.info("Saving Data to postgresql")
        base_path = "/Users/viktorpimentel/Documents/mutt_data/exam-victor-pimentel/data/process_data"
        save_path = "{coin}_{date}.csv".format(coin = coin, date = date.replace("-","_"))
        preprocess_data_path = os.path.join(base_path, save_path)

        saved_bucket = save_to_bucket(dataframe= data, save_path = preprocess_data_path)
        saved_sql = save_to_sql(dataframe=data,table_name = "history_data", columns = None)
        if saved_sql: print(f"Data raw saved for {coin}")

    except Exception as e: 
        logging.exception(f"Can not save data into postgresql due to: {e}")

    # Save Aggregated Data: 
    try: 
        logging.info("Creating Aggregating Data")
        data = agregate_data(query = "SELECT * FROM public.history_data", connection =create_engine_())
        data["upload_date"] = str(datetime.today)[:10]
        saved_sql = save_to_sql(dataframe=data,table_name = "aggregated_data", columns = [
            "price", 
            "coin_id", 
            "month", 
            "year", 
            "monthly_max", 
            "monthly_min", 
            "fecha_iso", 
            "fecha_unix"
        ], if_exists= "append")
        if saved_sql: print(f"Data Aggregated saved for {coin}")
    except Exception as e: 
        logging.exception(f"Can not save aggregated data into postgresql database")
```



Con un cronjob que tendrá la siguiente forma: 

```
0 3 * * * cd ~/Documents/project && bash execution.sh 
```

Así, la ejecución se llevará a cabo todos los días a las tres de la mañana

Dentro del archivo de ejecución bash se encontrará lo siguiente: 

```bash
/Users/viktorpimentel/miniforge3/bin/python3 /Users/viktorpimentel/Documents/mutt_data/exam-victor-pimentel/airflow/scripts/execution.py dayly price 1
```


Además, guarda los datos de manera local, tanto los datos raw de extracción como los procesados simulando un stage de Data Lake. Posteriormente envia los datos a la base de datos local

![image](images/databases.png)

## __Example Data__

1. Aggregated Data

![image](images/aggregated_data.jpg)

2. Historic Data

![image](images/history_data.jpg)

Una vez se tiene los datos  en la base de datos se puede comenzar con el trabajo de MAchine Learning

 ### __1. Bitcoin Price__

![image](images/bitcoin_price.jpg)


 ### __2. Cardano Price__

![image](images/cardano_price.jpg)

 ### __3. Ethereum Price__

![image](images/ethereum_price.jpg)